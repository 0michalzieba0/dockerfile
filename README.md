# Use an official Python runtime as a parent image
FROM ubuntu:16.04

# Define environment variable
ENV DEBIAN_FRONTEND noninteractive

# Install any needed packages
RUN echo "deb http://archive.ubuntu.com/ubuntu precise main universe" > /etc/apt/sources.list
RUN apt-get update
RUN apt-get install -y curl vim tree
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash
RUN apt-get install -y nodejs
RUN apt-get update

# Make port 80 available to the world outside this container
EXPOSE 80

# Create volume
VOLUME /home/mzieba

# Set the working directory to /mzieba
WORKDIR /home/mzieba

# Copy the current directory contents into the container 
ADD ./App /home/mzieba

# Install and run index.js when the container launches
CMD ["npm", "install"]
CMD ["npm", "start"]