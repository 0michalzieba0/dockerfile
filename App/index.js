"use strict";
const express = require('express');
const port = 80;
const app = express();

app.get('/', (req, res) => {
	res.send('Hello world');
});

app.listen(port, () => {
	console.log(`Server listen on port: ${port}`);
});